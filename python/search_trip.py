# -*- coding: utf-8 -*-

import base64
import hashlib
import re
import random
from argparse import ArgumentParser


#
# 2ch bbs 12-length trip code generation
#
def generate_trip_code(s_key, n_clip=12):
    hashed = base64.b64encode(hashlib.sha1(s_key.encode()).digest())
    s_trip = hashed.decode('utf-8')[:n_clip]
    s_trip = s_trip.translate(str.maketrans({'+': '.'}))
    return s_trip

# TODO:
# test performace of b64encode with altchars='./'
'''
base64.b64encode(s, altchars=None)¶
Encode the bytes-like object s using Base64 and return the encoded bytes.

Optional altchars must be a bytes-like object of at least length 2 (additional
characters are ignored) which specifies an alternative alphabet for the + and /
characters. This allows an application to e.g. generate URL or filesystem safe
Base64 strings. The default is None, for which the standard Base64 alphabet is
used.
'''


#
# clip or fill-zero to the given length
#
def clip_fill(s_in, length=12):
    if len(s_in) < length:
        s_in = '0' * (length - len(s_in)) + s_in  # fill 0
    elif len(s_in) > length:
        s_in = s_in[:length]
    return s_in


#
# N-base integer conversion class
#
class Base_N:

    #
    # Z85 encoding (modified)
    # '.-:+=^!/*?&<>()[]{}@%$#'
    #   & is replaced with _
    #   < is replaced with ,
    #   > is replaced with ;
    #   $ is replaced with ~
    s_digits = '0123456789' \
               'abcdefghijklmnopqrstuvwxyz' \
               'ABCDEFGHIJKLMNOPQRSTUVWXYZ' \
               '.-:+=^!/*?_,;()[]{}@%~#'

    def __init__(self, base=85):
        if base < 2 or base > 85:
            raise ValueError('base must be between 2 and 85.')
        self._base = base

    def to_str(self, num):
        if num == 0:
            return '0'

        result = ''
        while num:
            n = num % self._base
            result = Base_N.s_digits[n] + result
            num //= self._base
        return result

    def to_int(self, s):
        if len(s) == 0:
            return 0

        result = 0
        for c in s:
            if c in Base_N.s_digits:
                result = result * self._base + Base_N.s_digits.index(c)
            else:
                return 0
        return result


#
# test
#
def test(base=85):
    n_trial = 1000000
    n_range = base**12
    base_n = Base_N(base=base)

    for i in range(n_trial):
        x = int(random.random() * n_range)
        s_in = clip_fill(base_n.to_str(x))
        s_out = clip_fill(base_n.to_str(base_n.to_int(base_n.to_str(x))))
        if s_in != s_out:
            print(f'failed: x = {x}, s_in = [{s_in}], s_out = [{s_out}]')
    print('success!')


#
# main
#
if __name__ == '__main__':
    usage = 'Usage: python search_trip.py WORD'
    argparser = ArgumentParser(usage=usage)
    argparser.add_argument('word', type=str,
                           help='search word in the trip')
    argparser.add_argument('-d', '--delimiter', type=int,
                           default=1,
                           help='non-alphanumeric characters (default=1)')
    argparser.add_argument('-n', '--trials', type=int,
                           default=1000000,
                           help='number of trials (default=1000000)')
    argparser.add_argument('-b', '--base', type=int,
                           default=85,
                           help='base number for random number (default=85)')
    argparser.add_argument('-k', '--key-length', type=int,
                           default=12,
                           help='trip key length')
    argparser.add_argument('-v', '--verbose',
                           action='store_true',
                           help='verbose mode')
    argparser.add_argument('--test',
                           action='store_true',
                           help='enable Base_N test mode')
    args = argparser.parse_args()

    # verbose
    if args.verbose:
        print(f'search word = "{args.word}"')
        print(f'{args.delimiter} non-alphanumeric letters after the word')
        print(f'number of trials = {args.trials}')
        print(f'base number to generate random keys = {args.base}')
        print(f'length of keys = {args.key_length}')
        print(f'Base_N test mode : {args.test}')

    # generate regex string
    s_delimiter = r'\W' * args.delimiter
    regex = re.compile(r"{}{}.*".format(args.word, s_delimiter))

    base_n = Base_N(base=args.base)
    n_trial = args.trials
    n_range = args.base**12
    keys_found = []
    for n in range(n_trial):
        x = int(random.random() * n_range)
        s_in = base_n.to_str(x)
        s_in = clip_fill(s_in, length=args.key_length)

        if args.test:
            s_out = base_n.to_str(base_n.to_int(base_n.to_str(x)))
            s_out = clip_fill(s_out, length=args.key_length)
            if s_in != s_out:
                print(f'Warning: Base_N failed by {x}: {s_in} != {s_out}')
                continue

        s_trip = generate_trip_code(s_in)
        if args.verbose:
            print(f'trying key [{s_in}]: trip = [{s_trip}]')
        m = regex.match(s_trip)
        if m:
            keys_found.append((n, s_in, s_trip))
            print(f'Found!: key=[{s_in}], trip=[{s_trip}] at {n}-th trial.')

    if len(keys_found) == 0:
        print(f'Not found in {n_trial} trials.')
    else:
        print(f'{len(keys_found)} keys found in {n_trial} trials.')
        for k in keys_found:
            n, s_in, s_trip = k
            print(f'Found!: key=[{s_in}], trip=[{s_trip}] at {n}-th trial.')
