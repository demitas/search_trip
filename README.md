# search_trip.py

A small script to search keys for trip code starting with given word.

The program searches the trip code by generating random keys.
The longer word will take longer time.

The trip key length is fixed to 12 by default.


## usage

### Python

```shell
cd python
python search_trip.py WORD [options]
```

### JavaScript (node.js)

```shell
cd js
node search_trip_cui.js -t WORD [options]
```

### Javascript (browser) **Not working now**

  - setup

```shell
cd js
npm install
```

  - complie

```shell
cd js
bash make_browser_js.sh
http-server  # or any http server
# open localhost:8000
```

The build shell script needs `browserify`:

```shell
npm install -g browserify
```


### Javascript TODO

  - browser: make the input form in index.html
  - browser: measure operation time.
  - browser: stop/pause operation.
  - nice feature to make cpu usage limit.

### Rust

see [README.md in rust](./rust/README.md).

## options (Python)

- `-h` for detail options.

```
usage: Usage: python search_trip.py WORD

positional arguments:
  word                  search word in the trip

optional arguments:
  -h, --help            show this help message and exit
  -d DELIMITER, --delimiter DELIMITER
                        non-alphanumeric characters (default=1)
  -n TRIALS, --trials TRIALS
                        number of trials (default=1000000)
  -b BASE, --base BASE  base number for random number (default=85)
  -k KEY_LENGTH, --key-length KEY_LENGTH
                        trip key length
  -v, --verbose         verbose mode
  --test                enable Base_N test mode
```

- `-n` is the number of trials.

- `-d` specifies the number of trailing non-alphanumeric after the WORD.

- other options are for development and testing purpose.

### example

Search trip codes starting with `Me` with `2` trailing non-alphanumeric.
The number of trials = `20000000`.

```shell
$ python search_trip.py Me -d 2 -n 20000000
Found!: key=[A*e~P)?,(h7z], trip=[Me//6AS5h2i4] at 636340-th trial.
Found!: key=[gkwd{r%A@.z1], trip=[Me/.Cf7CiAzw] at 10096059-th trial.
Found!: key=[-bS(K.!w33Ni], trip=[Me/.tExhrXLh] at 10184185-th trial.
Found!: key=[q@RxnD/@Nxd}], trip=[Me..lPHMUubD] at 11372175-th trial.
```

## License

MIT license.
