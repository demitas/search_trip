# search_trip in Rust


## Build

 - build
```
cd search_trip
cargo build
```

## Usage

 - help for usage

```
cd search_trip
cargo run -- --help
```

 - run

```
cd search_trip
cargo run -- -n 100 -v abc
```

 - test

```
cd search_trip
cargo test
```
