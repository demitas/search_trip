mod base_n;
use crate::base_n::*;


//
// 2ch bbs 12-length trip code generation
//
fn generate_trip_code(s_key: &str, n_clip: u32) -> String {

    use sha1::{Sha1, Digest};
    //use base64;

    // create a Sha1 object
    let mut hasher = Sha1::new();

    // process input message to acquire hash digest.
    hasher.update((s_key).as_bytes());
    let hashed = hasher.finalize();
    let hash_base64 = base64::encode(hashed);

    // replace '+' with '.'
    let mut result = String::new();
    let mut s_base64 = hash_base64.chars();
    for _i in 0..n_clip as usize {
        let ch = s_base64.next().unwrap();  // assume long enough
        match ch {
            '+' => {result.push('.');},
            _   => {result.push(ch);},
        }
    }
    result
}


//
// command line options
//
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Opts {
    // positional args
    #[structopt(help="trip code to find")]
    tripcode: String,

    // flags
    #[structopt(short, long, help="verbose output")]
    verbose: bool,
    #[structopt(short="T", long="test", help="test mode")]
    test: bool,

    // optional args
    #[structopt(short="n", long, default_value="1000", help="number of trials")]
    trials: u64,
    #[structopt(short="d", long, default_value="1", help="number of delimiter chars")]
    delimiter: u32,
    #[structopt(short="b", long, default_value="85", help="base number for random")]
    base: u32,
    #[structopt(short="k", long, default_value="12", help="trip key length")]
    key_length: u32,
}


//
// main
//
fn main() {
    use rand::prelude::*;
    use regex::Regex;

    // parse command line options
    let opts = Opts::from_args();
    if opts.verbose {
        println!("tripcode = [{}]", opts.tripcode);
        println!("number of trials = [{}]", opts.trials);
    }

    // TODO: check validity of each arguments values
    let n_length = opts.key_length;
    let n_base = opts.base;
    let n_range = (n_base as u128).saturating_pow(n_length);
    let base_n = BaseN::new(n_base);

    let mut s_re = String::new();
    s_re.push('^');
    s_re.push_str(&opts.tripcode);
    for _i in 0..opts.delimiter {
        s_re.push_str(r"\W");
    }
    let re_trip = Regex::new(&s_re).unwrap();

    // trials to find a given trip code
    let mut rng = rand::thread_rng();

    let mut results = Vec::<(String, String)>::new();
    let mut trial = 0;
    while trial < opts.trials {
        trial += 1;

        // generate random number
        let x: u128 = rng.gen::<u128>() % n_range;

        // key input is zero-filled and clip to 12 chars
        let mut s = String::new();
        s.push_str("00000000000");
        s.push_str(&base_n.to_string(x));
        let s_in = String::from(&s[(s.len() - n_length as usize)..]);
        assert_eq!(x, base_n.to_int(&s_in));

        let s_out = generate_trip_code(&s_in, n_length);
        if re_trip.is_match(&s_out) {
            if opts.verbose {
                println!("key=[{}], trip=[{}]", s_in, s_out);
            }
            results.push((s_in, s_out));
        }
    }

    // print out results
    if results.len() == 0 {
        println!("not found in {} trials.", opts.trials);
    } else {
        for (k, v) in results {
            println!("key=[{}], trip=[{}]", k, v);
        }
    }
}
