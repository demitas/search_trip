// N-base integer conversion
pub struct BaseN {
    base: u128,
}

impl BaseN {
    const S_DIGITS: &'static [u8] =
                b"0123456789\
                abcdefghijklmnopqrstuvwxyz\
                ABCDEFGHIJKLMNOPQRSTUVWXYZ\
                .-:+=^!/*?_,;()[]{}@%~#";

    pub fn new(base :u32) -> BaseN {
        BaseN {
            base: base as u128,
        }
    }

    // convert number to string
    //
    fn mod_and_shift(&self, x :u128, s:&str) -> String {
        // convert x:u128 to char in S_DIGITS
        let mut s_out = String::new();
        s_out.push((BaseN::S_DIGITS[(x % self.base) as usize]) as char);
        s_out.push_str(&s);
        if x < self.base {
            s_out
        } else {
            self.mod_and_shift(x / self.base, &s_out)
        }
    }

    pub fn to_string(&self, num :u128) -> String {
        self.mod_and_shift(num, "")
    }

    // convert string to number
    //
    fn char_to_num(&self, ch: char) -> Option<u128> {
        for i in 0..self.base {
            if (BaseN::S_DIGITS[i as usize] as char) == ch {
                return Some(i);
            }
        }
        None  // invalid char
    }

    pub fn to_int(&self, s :&str) -> u128 {
        let mut result :u128 = 0;
        let mut c = s.chars();
        while let Some(ch) = c.next() {
            match self.char_to_num(ch) {
                None => {
                    panic!("Error!: invalid char of \'{ch}\' in \"{s}\"", ch=ch, s=s);
                },
                Some(x) => {
                    result *= self.base;
                    result += x;
                },
            }
        }
        result
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_to_string() {
        let base_n = BaseN::new(85);

        assert_eq!("0", base_n.to_string(0));
        assert_eq!("4", base_n.to_string(4));
        assert_eq!("a", base_n.to_string(10));
        assert_eq!("#", base_n.to_string(84));
        assert_eq!("10", base_n.to_string(85));
    }

    #[test]
    fn test_to_int() {
        let base_n = BaseN::new(85);

        assert_eq!(0, base_n.to_int("0"));
        assert_eq!(4, base_n.to_int("4"));
        assert_eq!(10, base_n.to_int("a"));
        assert_eq!(84, base_n.to_int("#"));
        assert_eq!(85, base_n.to_int("10"));
    }

    #[test]
    #[should_panic]
    fn test_to_int_invalid() {
        let base_n = BaseN::new(85);
        let _x = base_n.to_int("12>");
    }

    #[test]
    fn test_identy_conversion() {
        // random generator
        use rand::prelude::*;

        let n_length = 12;
        let n_base = 85;
        let n_range = (n_base as u128).saturating_pow(n_length);
        let base_n = BaseN::new(n_base);
        let mut rng = rand::thread_rng();

        for _i in 0..10000 {
            let x: u128 = rng.gen::<u128>() % n_range;

            // key input is zero-filled and clip to 12 chars
            let mut s = String::new();
            s.push_str("00000000000");
            s.push_str(&base_n.to_string(x));
            let s_in = &s[(s.len() - n_length as usize)..];
            let y = base_n.to_int(&s_in);
            assert_eq!(x, y);
        }
    }
}
