const yargs = require('yargs/yargs')

const mod_search_trip = require('./search_trip.js');
const search_trip = mod_search_trip.search_trip;

//
// main
//
// get the trip code from command line argument
//
const argv = yargs(process.argv.slice(2))
    .usage('Usage: $0 -t tripcode [options]')
    .example('$0 -t Hello -n 10000',
             'Search trip codes starting with "Hello" in 10000 trials.')
    .alias('t', 'tripcode')
    .string('t')
    .nargs('t', 1)
    .describe('t', 'Trip code to find.')
    .demandOption(['t'])
    .alias('n', 'n_trials')
    .number('n')
    .nargs('n', 1)
    .describe('n', 'The number of trials.')
    .default('n', 10000)
    .argv;

const trip_code = argv.tripcode;
const n_trials = argv.n_trials;
console.log(`looking for trip code [${trip_code}]`);
console.log(`trials = ${n_trials}`);

const results = search_trip(trip_code, n_trials);
if (results.length == 0) {
    console.log('not found.');
} else {
    for (const r of results) {
        key = r[0];
        trip = r[1];
        console.log(`found ${key} for ${trip}`);
    }
}

