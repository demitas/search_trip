const crypto = require('crypto');

module.exports = {
    search_trip: search_trip
}


//
// 2ch bbs 12-length trip code generation
//
function generate_trip_code(s_key, n_clip=12) {
    const regex = /\+/g;
    let hashed = crypto.createHash("sha1").update(s_key, "binary").digest("base64");
    hashed = hashed.slice(0, n_clip);
    let s_trip = hashed.replace(regex, '.');
    return s_trip;
}


//
// N-base integer conversion class
//
class Base_N {

    // Z85 encoding (modified)
    // '.-:+=^!/*?&<>()[]{}@%$#'
    //   & is replaced with _
    //   < is replaced with ,
    //   > is replaced with ;
    //   $ is replaced with ~

    static s_digits = '0123456789' +
               'abcdefghijklmnopqrstuvwxyz' +
               'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
               '.-:+=^!/*?_,;()[]{}@%~#'

    constructor(base=85) {
        this._base = base;
    }

    to_str(num) {
        if (num == 0) {
            return "0";
        }

        let result = "";
        while (num > 0) {
            let n = num % this._base;
            result = Base_N.s_digits[n] + result;
            num = Math.floor(num / this._base);
        }
        return result;
    }

    to_int(s) {
        if (s.length == 0) {
            return 0;
        }

        let result = 0;
        for (let c of s) {
            let i = Base_N.s_digits.indexOf(c);
            if (i == -1) {
                return 0;  // invalid character
            }
            result = result * this._base + i
        }
        return result;
    }
}

//
// search_trip
//
function search_trip(trip_code, n_trials) {
    const re_trip = new RegExp('^' + trip_code + '\\W');

    const base_n = new Base_N();
    const n_length = 12;
    const n_range = Math.pow(85, n_length);

    let results = [];

    for (let i_trial = 0; i_trial < n_trials; i_trial++) {
        let x = Math.random() * n_range;

        // key input is zero-filled and clip to 12 chars
        let s_in = ("00000000000" + base_n.to_str(x)).slice(-12);
        // generate trip
        let s_out = generate_trip_code(s_in);

        // regex match
        if (s_out.match(re_trip)) {
            results.push([s_in, s_out]);
            break;
        }
    }
    return results;
}
